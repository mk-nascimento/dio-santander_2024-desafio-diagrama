package iphone;

import iphone.models.IPhone;

public class Application {
    public static void main(String[] args) {
        IPhone iPhone = new IPhone();

        iPhone.adicionarNovaAba();
        iPhone.exibirPagina();
        iPhone.exibirPagina();

        iPhone.ligar();
        iPhone.atender();
        iPhone.iniciarCorreioVoz();

        iPhone.tocar();
        iPhone.selecionarMusica();
        iPhone.pausar();
    }
}
