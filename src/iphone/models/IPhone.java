package iphone.models;

import iphone.interfaces.*;

public class IPhone implements AparelhoTelefonico, NavegadorInternet, ReprodutorMusical {

    @Override
    public void tocar() {
        System.out.println("Tocando música..."); // NOSONAR
    }

    @Override
    public void pausar() {
        System.out.println("A música foi pausada!"); // NOSONAR
    }

    @Override
    public void selecionarMusica() {
        System.out.println("\"Linkin Park - In The End\" selecionado"); // NOSONAR
    }

    @Override
    public void exibirPagina() {
        System.out.println("Exibindo página na Web"); // NOSONAR
    }

    @Override
    public void adicionarNovaAba() {
        System.out.println("Adicionando nova aba ao navegador"); // NOSONAR
    }

    @Override
    public void atualizarPagina() {
        System.out.println("Atualizando página aberta"); // NOSONAR
    }

    @Override
    public void ligar() {
        System.out.println("Realizando chamada..."); // NOSONAR
    }

    @Override
    public void atender() {
        System.out.println("Ligação atendida."); // NOSONAR
    }

    @Override
    public void iniciarCorreioVoz() {
        System.out.println("Iniciando correios de voz"); // NOSONAR
    }

}
